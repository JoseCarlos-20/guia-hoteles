$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({ interval: 2000 });

  $('#contacto').on('show.bs.modal', function (e) {
    console.log('El modal contacto se está mostrado');

    $('#btnContacto').removeClass('btn-reserva');
    $('#btnContacto').addClass('btn-danger');
    $('#btnContacto').prop('disabled', true);


  });

  $('#contacto').on('shown.bs.modal', function (e) {
    console.log('El modal contacto se mostró ');
  });
  $('#contacto').on('hide.bs.modal', function (e) {
    console.log('El modal contacto se oculta ');
  });
  $('#contacto').on('hidden.bs.modal	', function (e) {
    console.log('El modal contacto se ocultó ');

    $('#btnContacto').removeClass('btn-danger');
    $('#btnContacto').addClass('btn-reserva ');
    $('#btnContacto').prop('disabled', false);
  });
});